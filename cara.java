/*
 *  Cara, X-efect for web
 *  Copyright (C) 1999 Radim Kolar <hsn@cybermail.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA or
 *  download copy from http://www.gnu.org/copyleft/gpl.html
 *
 */

import java.applet.Applet;
import java.awt.*;
import java.util.Vector;

/* mode 0 - horizontal left -> right */
/*      1 - horizontal ping-pong     */

public class cara extends Applet implements Runnable
{

 private Thread runner;
 private Color bgcolor;
 private Graphics graphics;
 private Image image;
 private int WIDTH,HEIGHT;
 private int SPEED;
 private int ew,eh,sp;
 private Vector colors;
 private int mode;
 
 // runtime status
 private int pos;
 private int st; // starting Y coordinate
 private int step;
 
   
 public final void init()
 {
  String s;
  runner=null;
  
  WIDTH=size().width;
  HEIGHT=size().height;
  
  s  = getParameter( "bgcolor");
  if( s != null )
        bgcolor=parseColorString(s);
            else
        bgcolor=Color.black;

  s = getParameter("speed");
  if(s==null) SPEED=150; else  SPEED=Integer.parseInt(s);

  s = getParameter("ew");
  if(s==null) ew=18; else  ew=Integer.parseInt(s);

  s = getParameter("eh");
  if(s==null) eh=4; else  eh=Integer.parseInt(s);
  
  s = getParameter("sp");
  if(s==null) sp=2; else  sp=Integer.parseInt(s);

  s = getParameter("mode");
  if(s==null) mode=0; else  mode=Integer.parseInt(s);
    
  if(ew>WIDTH) ew=WIDTH;
  if(eh>HEIGHT) eh=HEIGHT;
  st=(HEIGHT-eh)/2;
  step=sp+ew;
  pos=0;
  
  colors=new Vector();
  int i=1;
  while( (s=getParameter("color"+(i++)))!=null)
    colors.addElement(parseColorString(s));
  
  if(colors.size()==0)
   {
    colors.addElement(parseColorString("#003300"));                
    colors.addElement(parseColorString("#006600"));            
    colors.addElement(parseColorString("#009900"));        
    colors.addElement(parseColorString("#00CC00"));
    colors.addElement(parseColorString("#33ff00"));
   }   
        
 image = createImage(WIDTH, HEIGHT);
 graphics = image.getGraphics();
  
 } 
 
 public void paint(Graphics g)
 {
    update(g);
 }

 public void update(Graphics g)
 {
  // System.out.println("pos="+pos+" step="+step);
  graphics.setColor(bgcolor);
  graphics.fillRect(0, 0, WIDTH, HEIGHT);
  int x=pos;
  int cs=colors.size();
  int xstep=step;
  for(int i=0;i<cs;i++)
   {
    graphics.setColor( (Color) colors.elementAt(i));
    graphics.fillRect(x,st,ew,eh);
    x+=xstep;
    if(x>WIDTH-xstep) if(mode==0) x=0; else { xstep=-xstep;x+=2*xstep;}
     else
    if(x<0) { xstep=-xstep;x=xstep;/*step;*/}
   }
   pos+=step;
   if(pos>WIDTH-step) if(mode==0) pos=0; else {step=-step;pos+=2*step;}
    else
   if(pos<0) {step=-step;pos=step;/*step*/}
 
  g.drawImage(image, 0, 0, this);
 }

  public final String getAppletInfo()
  {
   return "Cara 1.0, simple X-efect for web page\n(C) Copyright by Radim Kolar (hsn@cybermail.net).\nFree software under GPL copyleft.";
  }  

  /*  
     * String pinfo[][] = {
     *	 {"fps",    "1-10",    "frames per second"},
     *	 {"repeat", "boolean", "repeat image loop"},
     *	 {"imgs",   "url",     "images directory"}
     * };
     * </pre></blockquote>
     * <p>
     * The implementation of this method provided by the 
     * <code>Applet</code> class returns <code>null</code>. 
     *
     * @return  an array describing the parameters this applet looks for.
     * @since   JDK1.0
     */
    public final String[][] getParameterInfo() {
        String pinfo[][]={
         {"speed","integer time (ms)","delay between frames"},
         {"bgcolor","#rrggbb","background color"},
         {"ew","integer (pixels)","block width"},
         {"eh","integer (pixels)","block height"},
         {"sp","integer (pixels)","space between blocks"},
         {"mode","0-1","tick-tack/roll efect"},
         {"color1","#rrggbb","color of 1st block"},
         {"colorn","#rrggbb","color of nth block"}         
        };
	return pinfo;
    }
  
  

 public final void run()
 {
        while (runner != null)
        {
            repaint();

            try
            {
                    Thread.sleep(SPEED);
            }
            catch (InterruptedException e)
            {
                // do nothing
            }
        }
 }
 
    public void start()
    {
        // user visits the page, create a new thread

        if (runner == null)
        {
            runner = new Thread(this);
            runner.start();
        }
    }
    
    public void stop()
    {
        // user leaves the page, stop the thread

        if (runner != null && runner.isAlive())
            runner.stop();

        runner = null;
    }
    
    private Color parseColorString(String colorString)
    {
      if(colorString.startsWith("#")) colorString=colorString.substring(1);
        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }
 
}